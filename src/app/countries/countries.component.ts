import { Component, OnInit } from '@angular/core';
import { CountriesService } from './countries.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styles: []
})
export class CountriesComponent implements OnInit {
    public result:any;
  constructor(private countries:CountriesService) { }

  ngOnInit() {
    this.countries.getCoutries().subscribe((posRes)=>{
                    this.result=posRes;
    },(errRes:HttpErrorResponse)=>{
          console.log(errRes);
    })
  }

}


