import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
@Injectable({
  providedIn: 'root'
})
export class CountriesService {

  constructor(private http:HttpClient) { }
  public getCoutries():Observable<any>{
    return this.http.get("http://restcountries.eu/rest/v2/all");
  }
}
