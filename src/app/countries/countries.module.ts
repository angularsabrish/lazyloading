import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CountriesComponent } from './countries.component';

import { HttpClientModule } from '@angular/common/http';
import { Routes,RouterModule } from '@angular/router';

export const approutes:Routes=[{path:"",component:CountriesComponent}]

@NgModule({
  declarations: [CountriesComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule.forChild(approutes)
  ],
  providers: [],
  exports:[CountriesComponent]

})
export class CountriesModule { }

