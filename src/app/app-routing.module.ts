
import { Routes, RouterModule } from '@angular/router';
import {PageoneComponent} from './pageone/pageone.component';
import {  ModuleWithProviders } from '@angular/core';
export const app: Routes = [{path:'page_one',component:PageoneComponent},
                        {path:'lazy',loadChildren:"./countries/countries.module#CountriesModule"}]


export const lazyroutes:ModuleWithProviders=RouterModule.forRoot(app);
