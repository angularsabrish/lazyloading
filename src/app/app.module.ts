import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {  lazyroutes } from './app-routing.module';
import { AppComponent } from './app.component';
import { PageoneComponent } from './pageone/pageone.component';
import { HttpClientModule } from '@angular/common/http'


@NgModule({
  declarations: [
    AppComponent,
    PageoneComponent
  ],
  imports: [
    BrowserModule,
    lazyroutes,HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
